<deployment-project plugin="plugin.toolbox" plugin-version="1.0">
  <configuration build-checksum="1781719379" file="C:\Users\zaber\Documents\MATLAB\zaber_device_control_toolbox\Zaber Device Control Toolbox.prj" location="C:\Users\zaber\Documents\MATLAB\zaber_device_control_toolbox" name="Zaber Device Control Toolbox" target="target.toolbox" target-name="Package Toolbox">
    <param.appname>Zaber Device Control Toolbox</param.appname>
    <param.authnamewatermark>Zaber Software Team</param.authnamewatermark>
    <param.email>contact@zaber.com</param.email>
    <param.company>Zaber Technologies</param.company>
    <param.summary>Control Zaber motion devices from MATLAB.</param.summary>
    <param.description>Provides classes and enumerations to assist with use of Zaber motion devices. Supports both Zaber protocols and provides both protocol-specific interfaces and a generic interface for protocol-agnostic use of basic functionality. Unit of measure conversions are provided for many devices via a lookup table, which can be updated via an included Python script to pick up newly released product information.

This toolbox is deprecated; please use the Zaber Motion Library in new projects: https://www.mathworks.com/matlabcentral/fileexchange/72236-zaber-motion-library</param.description>
    <param.screenshot>${PROJECT_ROOT}\Zaber Device Control Toolbox.png</param.screenshot>
    <param.version>1.2.3</param.version>
    <param.output>${PROJECT_ROOT}\Zaber Device Control Toolbox.mltbx</param.output>
    <param.products.name />
    <param.products.id />
    <param.products.version />
    <param.platforms />
    <param.guid>10df15ed-7a34-47dc-b4b9-e4647f73304b</param.guid>
    <param.exclude.filters>% List files contained in your toolbox folder that you would like to exclude
% from packaging.  Excludes should be listed relative to the toolbox folder.
% Some examples of how to specify excludes are provided below:
%
% A single file in the toolbox folder:
% .svn
%
% A single file in a subfolder of the toolbox folder:
% example/.svn
%
% All files in a subfolder of the toolbox folder:
% example/*
%
% All files of a certain name in all subfolders of the toolbox folder:
% **/.svn
%
% All files matching a pattern in all subfolders of the toolbox folder:
% **/*.bak
%
.git
.gitignore
.gitlab-ci.yml</param.exclude.filters>
    <param.exclude.pcodedmfiles>true</param.exclude.pcodedmfiles>
    <param.examples>&lt;?xml version="1.0" encoding="utf-8"?&gt;
&lt;examples&gt;
   &lt;exampleCategory name="zaber_device_control_toolbox"&gt;
      &lt;example name="Example_Advanced" type="html"&gt;
         &lt;file type="source"&gt;/html/Example_Advanced.html&lt;/file&gt;
         &lt;file type="main"&gt;/Example_Advanced.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="Example_Ascii" type="html"&gt;
         &lt;file type="source"&gt;/html/Example_Ascii.html&lt;/file&gt;
         &lt;file type="main"&gt;/Example_Ascii.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="Example_Binary" type="html"&gt;
         &lt;file type="source"&gt;/html/Example_Binary.html&lt;/file&gt;
         &lt;file type="main"&gt;/Example_Binary.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="Example_FilterWheel" type="html"&gt;
         &lt;file type="source"&gt;/html/Example_FilterWheel.html&lt;/file&gt;
         &lt;file type="main"&gt;/Example_FilterWheel.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="Example_IO" type="html"&gt;
         &lt;file type="source"&gt;/html/Example_IO.html&lt;/file&gt;
         &lt;file type="main"&gt;/Example_IO.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="Example_Minimal" type="html"&gt;
         &lt;file type="source"&gt;/html/Example_Minimal.html&lt;/file&gt;
         &lt;file type="main"&gt;/Example_Minimal.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
   &lt;/exampleCategory&gt;
   &lt;exampleCategory name="documentation"&gt;
      &lt;example name="introduction" type="html"&gt;
         &lt;file type="source"&gt;/documentation/html/introduction.html&lt;/file&gt;
         &lt;file type="main"&gt;/documentation/introduction.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="userguide" type="html"&gt;
         &lt;file type="source"&gt;/documentation/html/userguide.html&lt;/file&gt;
         &lt;file type="main"&gt;/documentation/userguide.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
   &lt;/exampleCategory&gt;
   &lt;exampleCategory name="+Zaber"&gt;
      &lt;example name="AsciiDevice" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/AsciiDevice.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/AsciiDevice.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="AsciiIoPort" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/AsciiIoPort.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/AsciiIoPort.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="AsciiMessage" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/AsciiMessage.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/AsciiMessage.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="AsciiProtocol" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/AsciiProtocol.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/AsciiProtocol.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="BinaryCommandType" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/BinaryCommandType.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/BinaryCommandType.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="BinaryDevice" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/BinaryDevice.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/BinaryDevice.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="BinaryErrorType" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/BinaryErrorType.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/BinaryErrorType.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="BinaryIoPort" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/BinaryIoPort.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/BinaryIoPort.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="BinaryMessage" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/BinaryMessage.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/BinaryMessage.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="BinaryProtocol" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/BinaryProtocol.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/BinaryProtocol.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="BinaryReplyType" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/BinaryReplyType.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/BinaryReplyType.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="BinaryStatusType" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/BinaryStatusType.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/BinaryStatusType.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="Device" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/Device.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/Device.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="DeviceDatabase" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/DeviceDatabase.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/DeviceDatabase.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="DeviceUnits" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/DeviceUnits.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/DeviceUnits.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="IoPort" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/IoPort.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/IoPort.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="MessageType" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/MessageType.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/MessageType.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="MotionType" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/MotionType.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/MotionType.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
      &lt;example name="Protocol" type="html"&gt;
         &lt;file type="source"&gt;/+Zaber/html/Protocol.html&lt;/file&gt;
         &lt;file type="main"&gt;/+Zaber/Protocol.m&lt;/file&gt;
         &lt;file type="thumbnail"/&gt;
      &lt;/example&gt;
   &lt;/exampleCategory&gt;
&lt;/examples&gt;</param.examples>
    <param.demosxml />
    <param.apps />
    <param.registered.apps />
    <param.docs>${PROJECT_ROOT}\info.xml</param.docs>
    <param.getting.started.guide />
    <param.matlabpath.excludes />
    <param.javaclasspath.excludes />
    <param.exported.on.package>false</param.exported.on.package>
    <param.release.start>R2016a</param.release.start>
    <param.release.end>latest</param.release.end>
    <param.release.current.only>false</param.release.current.only>
    <param.compatiblity.windows>true</param.compatiblity.windows>
    <param.compatiblity.macos>true</param.compatiblity.macos>
    <param.compatiblity.linux>true</param.compatiblity.linux>
    <param.compatiblity.matlabonline>false</param.compatiblity.matlabonline>
    <unset>
      <param.output />
      <param.products.name />
      <param.products.id />
      <param.products.version />
      <param.platforms />
      <param.exclude.pcodedmfiles />
      <param.demosxml />
      <param.apps />
      <param.registered.apps />
      <param.getting.started.guide />
      <param.matlabpath.excludes />
      <param.javaclasspath.excludes />
      <param.exported.on.package />
      <param.release.current.only />
      <param.compatiblity.windows />
      <param.compatiblity.macos />
      <param.compatiblity.linux />
    </unset>
    <fileset.rootdir>
      <file>C:\Users\zaber\Documents\MATLAB\zaber_device_control_toolbox</file>
    </fileset.rootdir>
    <fileset.rootfiles>
      <file>${PROJECT_ROOT}\+Zaber</file>
      <file>${PROJECT_ROOT}\CHANGELOG</file>
      <file>${PROJECT_ROOT}\demos.xml</file>
      <file>${PROJECT_ROOT}\documentation</file>
      <file>${PROJECT_ROOT}\Example_Advanced.m</file>
      <file>${PROJECT_ROOT}\Example_Ascii.m</file>
      <file>${PROJECT_ROOT}\Example_Binary.m</file>
      <file>${PROJECT_ROOT}\Example_FilterWheel.m</file>
      <file>${PROJECT_ROOT}\Example_IO.m</file>
      <file>${PROJECT_ROOT}\Example_Minimal.m</file>
      <file>${PROJECT_ROOT}\html</file>
      <file>${PROJECT_ROOT}\info.xml</file>
      <file>${PROJECT_ROOT}\README.md</file>
      <file>${PROJECT_ROOT}\tests</file>
      <file>${PROJECT_ROOT}\Zaber Device Control Toolbox.png</file>
    </fileset.rootfiles>
    <fileset.depfun.included />
    <fileset.depfun.excluded />
    <fileset.package />
    <build-deliverables>
      <file location="C:\Users\zaber\Documents\MATLAB\zaber_device_control_toolbox" name="Zaber Device Control Toolbox.mltbx" optional="false">C:\Users\zaber\Documents\MATLAB\zaber_device_control_toolbox\Zaber Device Control Toolbox.mltbx</file>
    </build-deliverables>
    <workflow />
    <matlab>
      <root>C:\Program Files\MATLAB\R2018a</root>
      <toolboxes />
    </matlab>
    <platform>
      <unix>false</unix>
      <mac>false</mac>
      <windows>true</windows>
      <win2k>false</win2k>
      <winxp>false</winxp>
      <vista>false</vista>
      <linux>false</linux>
      <solaris>false</solaris>
      <osver>10.0</osver>
      <os32>false</os32>
      <os64>true</os64>
      <arch>win64</arch>
      <matlab>true</matlab>
    </platform>
  </configuration>
</deployment-project>