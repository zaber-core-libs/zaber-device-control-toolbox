This Zaber Technology's first official interfacing toolbox for MATLAB. 
This package is now deprecated; please use the 
[Zaber Motion Library](https://www.mathworks.com/matlabcentral/fileexchange/72236-zaber-motion-library)
instead. Existing users can still receive support, and major bugs will
be fixed.

The code in the +Zaber folder provides facilities for detecting and 
using all Zaber motion devices from inside MATLAB. 

See the various Example_*.m files for example usage. More details about 
features and usage are available in the documentation.

Email contact@zaber.com for inquiries or support.
